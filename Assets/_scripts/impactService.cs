﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impactService {

	public List<string> myobjects = new List<string>();

	public List<string> getAnswer(string answer,int currentQuestion){
		int response = 0;
		string responseText = "";

		if (currentQuestion == 0) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward1Level3";
			} else {
				response = 0;
				responseText = "error1Level3";
			}
		}

		if (currentQuestion == 1) {
			GameManager.Instance.QuestionsStatus.Clear ();

			bool responseA = false;
			bool responseB = false;
			bool responseC = false;
			bool responseD = false;

			string[] answerArray = answer.Split (',');
			int probabilityOcurrance1 = 5;
			int costImpactLowerBound1 = 5;
			int costImpactUpperBound1 = 5;
			int probabilityOcurrance2 = 5;
			int costImpactLowerBound2 = 5;
			int costImpactUpperBound2 = 5;
			int probabilityOcurrance3 = 5;
			int costImpactLowerBound3 = 5;
			int costImpactUpperBound3 = 5;
			int probabilityOcurrance4 = 5;
			int costImpactLowerBound4 = 5;
			int costImpactUpperBound4 = 5;

			int enteredAnswerA1 = int.Parse(answerArray[0]);
			int enteredAnswerA2 = int.Parse(answerArray[1]);
			int enteredAnswerA3 = int.Parse(answerArray[2]);
			int enteredAnswerB1 = int.Parse(answerArray[3]);
			int enteredAnswerB2 = int.Parse(answerArray[4]);
			int enteredAnswerB3 = int.Parse(answerArray[5]);
			int enteredAnswerC1 = int.Parse(answerArray[6]);
			int enteredAnswerC2 = int.Parse(answerArray[7]);
			int enteredAnswerC3 = int.Parse(answerArray[8]);
			int enteredAnswerD1 = int.Parse(answerArray[9]);
			int enteredAnswerD2 = int.Parse(answerArray[10]);
			int enteredAnswerD3 = int.Parse(answerArray[11]);

			responseA = getResponse (probabilityOcurrance1, costImpactLowerBound1, costImpactUpperBound1, enteredAnswerA1, enteredAnswerA2, enteredAnswerA3);
			responseB = getResponse (probabilityOcurrance2, costImpactLowerBound2, costImpactUpperBound2, enteredAnswerB1, enteredAnswerB2, enteredAnswerB3);
			responseC = getResponse (probabilityOcurrance3, costImpactLowerBound3, costImpactUpperBound3, enteredAnswerC1, enteredAnswerC2, enteredAnswerC3);
			responseD = getResponse (probabilityOcurrance4, costImpactLowerBound4, costImpactUpperBound4, enteredAnswerD1, enteredAnswerD2, enteredAnswerD3);

			if (!responseA || !responseB || !responseC || !responseD ) {
				response = 0;
				responseText = "error2Level2";
			} else {
				response = 1;
				responseText = "reward2Level2";
			}
		}

		if (currentQuestion == 2) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward3Level3";
			} else {
				response = 0;
				responseText = "error3Level3";
			}
		}

		if (currentQuestion == 3) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward4Level3";
			} else {
				response = 0;
				responseText = "error4Level3";
			}
		}

		if (currentQuestion == 4) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward5Level3";
			} else {
				response = 0;
				responseText = "error5Level3";
			}
		}

		if (currentQuestion == 5) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward6Level3"+","+"endGameText7"+","+"endGameText3"+","+"endGameText4"+","+"endGameText5"+","+"endGameText6";

			} else {
				response = 0;
				responseText = "error6Level3"+","+"endGameText7"+","+"endGameText3"+","+"endGameText4"+","+"endGameText5"+","+"endGameText6";
			}
		}

		myobjects.Add (response.ToString ());
		myobjects.Add (responseText);
		return myobjects;
	}
	bool getResponse(int probabilityOcurrance, int costImpactLowerBound, int costImpactUpperBound, int enteredAnswer1, int enteredAnswer2, int enteredAnswer3){
		bool response = false;
		if (enteredAnswer1 == probabilityOcurrance && enteredAnswer2 == costImpactLowerBound && enteredAnswer3 == costImpactUpperBound) {
			response = true;
			GameManager.Instance.QuestionsStatus.Add (response.ToString());
		} else {
			response = false;
			GameManager.Instance.QuestionsStatus.Add (response.ToString());
		}
		return response;
	}
	public bool getImpactDemo(List<int> answer){
		if (answer [0] >= 95 && answer [1] >= 95) {
			return true;
		} else {
			return false;
		}
	}
}
