﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class playerNameGetter : MonoBehaviour {

	void Start () {
		this.gameObject.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.sessionStorage.onGetUserName ();
	}
}
