﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour {
	public static QuestionManager Instance;
	public List<GameObject> questions = new List<GameObject>();
	public List<int> questionType = new List<int>();
	public int index = 0;

	void Start(){
		Instance = this;
	}

	public void enableQuestion(){
		questions [index].SetActive (false);
		index += 1;
		if (questions [index] != null) {
			questions [index].SetActive (true);
		}
	}
	public void enableDemoQuestion(){
		questions [index].SetActive (false);
		questions [index+1].SetActive (true);
		index += 1;
	}
}
