﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class promotionLoadNextLevel : MonoBehaviour {
	public static promotionLoadNextLevel Instance;

	void Start(){
		Instance = this;
	}

	public void loadNextScene(){
		//int sceneIndex = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene ("LevelSelect");
	}
	public void loadSceneSelect(){
		SceneManager.LoadScene ("LevelSelect");
	}
	public void loadSelectedLevel(int level){
		SceneManager.LoadScene (level);
	}
}
