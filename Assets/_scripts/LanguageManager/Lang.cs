﻿using System;
using System.Collections;
using System.IO;
using System.Xml;

using UnityEngine;

public class Lang {

	private Hashtable Strings;
	public Lang ( string path,string language,bool web)
	{
		if (!web) {
			setLanguage(path, language);
		} else {
			setLanguageWeb(path, language);
		}
	}

	public void setLanguage ( string path, string language) {
		TextAsset textAsset = (TextAsset)Resources.Load("languages", typeof(TextAsset));
		XmlDocument xml = new XmlDocument ();
		xml.LoadXml ( textAsset.text );

		Strings = new Hashtable();
		var element = xml.DocumentElement[language];
		if (element != null) {
			var elemEnum = element.GetEnumerator();
			while (elemEnum.MoveNext()) {
				Strings.Add((elemEnum.Current as XmlElement).GetAttribute("name"),(elemEnum.Current as XmlElement).InnerText);
			}
		} else {
			Debug.LogError("The specified language does not exist: " + language);
		}
	}

	public void setLanguageWeb ( string xmlText, string language) {
		var xml = new XmlDocument();
		xml.Load(new StringReader(xmlText));

		Strings = new Hashtable();
		var element = xml.DocumentElement[language];
		if (element != null) {
			var elemEnum = element.GetEnumerator();
			while (elemEnum.MoveNext()) {
				var xmlItem = (XmlElement)elemEnum.Current;
				Strings.Add(xmlItem.GetAttribute("name"), xmlItem.InnerText);
			}
		} else {
			Debug.LogError("The specified language does not exist: " + language);
		}
	}

	public string getString (string name) {
		if (!Strings.ContainsKey(name)) {
			Debug.LogError("The specified string does not exist: " + name);

			return "";
		}

		return (string)Strings[name];
	}

}
