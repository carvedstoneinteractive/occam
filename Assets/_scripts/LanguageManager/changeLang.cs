﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class changeLang : MonoBehaviour {
	public static changeLang Instance;
	public Lang LMan;
	public string currentLang = "en";

	void Awake(){
		Instance = this;
	}

	void OnEnable()
	{
		
		if(PlayerPrefs.GetString("language")!="")
		{
			string selectedLang = PlayerPrefs.GetString("language");
			if(selectedLang=="")
			{
				selectedLang="en";
			}
			LMan = new Lang(Path.Combine(Application.dataPath, "languages.xml"), selectedLang, false);
		}else{
			LMan = new Lang(Path.Combine(Application.dataPath, "languages.xml"), currentLang, false);
			PlayerPrefs.SetString("language","en");
		}

	}

	public void setLang(string selectedLang)
	{
		currentLang = selectedLang;
		LMan.setLanguage(Path.Combine(Application.dataPath, "languages.xml"), selectedLang);	
		PlayerPrefs.SetString("language",selectedLang);
		SceneManager.LoadScene (0);
	}

}
