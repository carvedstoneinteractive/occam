﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;
	public int playerChips = 200;
	public int placedBet = 0;
	public sessionStorage sessionStorage;
	probabilityService probabilityService;
	impactService impactService;
	demoService demoService;
	confidenceService confidenceService;
	public Text userName;
	public GameObject nameCollector;
	public GameObject humbleMomentObject;
	public GameObject rewardMomentObject;
	public GameObject promotionObject;
	public GameObject ErrorManager;
	public GameObject chipsBarObject;
	public AudioClip promotionSound;
	public string selectedAnswer;
	public int currentQuestion;
	public int demoQuestion = 0;
	int responseTrue = 1;
	int responseFalse = 0;
	public List<string> responses = new List<string>();
	public List<string> level1Answers = new List<string>();
	public List<string> QuestionsStatus = new List<string>();
	public Color colorGreen = Color.green;
	public Color colorRed = Color.red;

	public void Awake(){
		ButtonManager.OnSendAnswer += this.GetLevelQuestions;
	}

	public void OnDestroy(){
		ButtonManager.OnSendAnswer -= this.GetLevelQuestions;
	}

	void GetLevelQuestions(){
		int sceneIndex = SceneManager.GetActiveScene ().buildIndex;
		if (sceneIndex == 3) {
			currentQuestion = QuestionManager.Instance.index;
			probabilityReceiver (selectedAnswer);
		}

		if (sceneIndex == 4) {
			currentQuestion = QuestionManager.Instance.index;
			confidenceReceiver (selectedAnswer);
		}

		if (sceneIndex == 5) {
			currentQuestion = QuestionManager.Instance.index;
			impactReceiver (selectedAnswer);
		}

	}

	void Start () {
		sessionStorage = new sessionStorage ();

		SceneManager.sceneLoaded += this.OnLoadCallback;
		Instance = this;
		DontDestroyOnLoad (this.gameObject);
		PlayerPrefs.DeleteAll ();
	}

	void OnLoadCallback(Scene scene, LoadSceneMode sceneMode){
		impactService = new impactService ();
		probabilityService = new probabilityService ();
		demoService = new demoService ();
		confidenceService = new confidenceService ();
	}

	public void setUsername(){
		sessionStorage.onSetUserName (userName.text);
	}

	public void checkUserName(){
		if (sessionStorage.onGetUserName () == "") {
			nameCollector.SetActive (true);
		} else {
			SceneManager.LoadScene (1);
		}
	}

	public void demoReceiver(string answer){
		demoQuestion = QuestionManager.Instance.index;
		responses.Clear ();
		SetErrorManager ();
		responses = demoService.getAnswer (answer,demoQuestion);
		if (int.Parse(responses[0])==responseTrue) {
			SetRewardScreen (responses[1]);
		} else {
			SetErrorScreen (responses [1]);
		}
	}

	public void probabilityReceiver(string answer){
		responses.Clear ();
		SetErrorManager ();
		responses = probabilityService.getAnswer (answer,currentQuestion);
		if (int.Parse(responses[0])==responseTrue) {
			SetRewardScreen (responses [1]);
			if (QuestionManager.Instance.index == QuestionManager.Instance.questions.Count-1) {
				GameManager.Instance.sessionStorage.onSetLevel2Result();
				promotionReceiver ();
			}
		} else {
			SetErrorScreen (responses[1]);
		}
	}

	public void confidenceReceiver(string answer){
		responses.Clear ();
		SetErrorManager ();
		responses = confidenceService.getAnswer (answer,currentQuestion);

		if (int.Parse(responses[0])==responseTrue) {
			for (int i = 0; i < QuestionsStatus.Count; i++) {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].text = QuestionsStatus [i];
			}
			SetTextGR ();
			SetRewardScreen (responses [1]);
			if (QuestionManager.Instance.index == QuestionManager.Instance.questions.Count-1) {
				GameManager.Instance.sessionStorage.onSetLevel3Result();
				promotionReceiver ();
			}
		} else {
			for (int i = 0; i < QuestionsStatus.Count; i++) {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].text = QuestionsStatus [i];
			}
			SetTextGR ();
			SetErrorScreen (responses[1]);
		}
	}

	public void impactReceiver(string answer){
		responses.Clear ();
		SetErrorManager ();
		QuestionsStatus.Clear ();
		responses = impactService.getAnswer (answer,currentQuestion);

		if (int.Parse(responses[0])==responseTrue) {
			for (int i = 0; i < QuestionsStatus.Count; i++) {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].text = QuestionsStatus [i];
			}
			SetTextGR ();
			SetRewardScreen (responses [1]);
			if (QuestionManager.Instance.index == QuestionManager.Instance.questions.Count-1) {
				GameManager.Instance.sessionStorage.onSetLevel3Result();
				EndGame ();
			}
		} else {
			for (int i = 0; i < QuestionsStatus.Count; i++) {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].text = QuestionsStatus [i];
			}
			SetTextGR ();
			SetErrorScreen (responses[1]);
		}
	}

	public void impactReceiverDemo(List<int> answer){
		
		if (!impactService.getImpactDemo (answer)) {
			humbleMomentObject.SetActive (true);
			humbleMomentObject.transform.SetAsLastSibling ();
			ErrorManager.GetComponent<errorManager> ().getLocalizedError ("error3Demo");
		} else {
			rewardMomentObject.SetActive (true);
			rewardMomentObject.transform.SetAsLastSibling ();
			ErrorManager.GetComponent<errorManager> ().getLocalizedReward ("reward3Demo");
			promotionReceiver ();
		}
	}

	public void promotionReceiver(){
		promotionObject = ErrorManager.GetComponent<errorManager> ().promotionObject;
		promotionObject.SetActive (true);
		promotionObject.transform.SetAsLastSibling ();
		GetComponent<AudioSource> ().PlayOneShot (promotionSound);
	}

	void EndGame(){
//		rewardMomentObject.SetActive (false);
		ErrorManager.GetComponent<errorManager> ().endGameObject.SetActive (true);
		ErrorManager.GetComponent<errorManager> ().endGameObject.transform.SetAsLastSibling ();

	}

	void SetErrorManager (){
		ErrorManager = GameObject.Find ("ErrorManager");
		humbleMomentObject = ErrorManager.GetComponent<errorManager> ().humbleMomentObject;
		rewardMomentObject = ErrorManager.GetComponent<errorManager> ().rewardMomentObject;
	}

	void SetErrorScreen(string response){
		humbleMomentObject.SetActive (true);
		humbleMomentObject.transform.SetAsLastSibling ();
		ErrorManager.GetComponent<errorManager> ().getLocalizedError (response);
	}

	void SetRewardScreen(string response){
		rewardMomentObject.SetActive (true);
		rewardMomentObject.transform.SetAsLastSibling ();
		ErrorManager.GetComponent<errorManager> ().getLocalizedReward (response);
		GetComponent<AudioSource> ().PlayOneShot (promotionSound);
	}

	void SetTextGR(){
		for (int i = 0; i < QuestionsStatus.Count; i++) {
			if (QuestionsStatus [i] == "True") {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].color = colorGreen;
			} else {
				ErrorManager.GetComponent<errorManager> ().level2Q7Answers [i].color = colorRed;
			}
		}
	}


}
