﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class confidenceService {

	public List<string> myobjects = new List<string>();

	public List<string> getAnswer(string answer,int currentQuestion){
		int response = 0;
		string responseText = "";

		if (currentQuestion == 0) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1 || int.Parse(answerArray[2])==1 ) {
				response = 1;
				responseText = "reward1Level2";
			} else {
				response = 0;
				responseText = "error1Level2";
			}
		}

		if (currentQuestion == 1) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward2Level2";
			} else {
				response = 0;
				responseText = "error2Level2";
			}
		}

		if (currentQuestion == 2) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==0) {
				response = 1;
				responseText = "reward3Level2";
			} else {
				response = 0;
				responseText = "error3Level2";
			}
		}

		if (currentQuestion == 3) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==0) {
				response = 1;
				responseText = "reward4Level2";
			} else {
				response = 0;
				responseText = "error4Level2";
			}
		}

		if (currentQuestion == 4) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward5Level2";
			} else {
				response = 0;
				responseText = "error5Level2";
			}
		}

		if (currentQuestion == 5) {
			string[] answerArray = answer.Split (',');
			if (float.Parse (answerArray [0]) >= 0.45f && float.Parse (answerArray [0]) <= 0.50f) {
				response = 1;
				responseText = "reward6Level2";
			} else {
				response = 0;
				responseText = "error6Level2";
			}
		}

		if (currentQuestion == 6) {
			string[] answerArray = answer.Split (',');
			if (float.Parse (answerArray [0]) >= 0.45f && float.Parse (answerArray [0]) <= 0.50f) {
				response = 1;
				responseText = "reward7Level2";
			} else {
				response = 0;
				responseText = "error7Level2";
			}
		}

		if (currentQuestion == 7) {
			GameManager.Instance.QuestionsStatus.Clear ();

			bool responseA = false;
			bool responseB = false;
			bool responseC = false;
			bool responseD = false;
			bool responseE = false;

			string[] answerArray = answer.Split (',');
			int expectedAnswerA = 9;
			int expectedAnswerAA = 10;
			int expectedAnswerB = 90;
			int expectedAnswerBB = 102;
			int expectedAnswerC = 60;
			int expectedAnswerCC = 70;
			int expectedAnswerD = 25000;
			int expectedAnswerDD = 26407;
			int expectedAnswerE = 2;
			int expectedAnswerEE = 4;

			int enteredAnswerA = int.Parse(answerArray[0]);
			int enteredAnswerAA = int.Parse(answerArray[1]);
			int enteredAnswerB = int.Parse(answerArray[2]);
			int enteredAnswerBB = int.Parse(answerArray[3]);
			int enteredAnswerC = int.Parse(answerArray[4]);
			int enteredAnswerCC = int.Parse(answerArray[5]);
			int enteredAnswerD = int.Parse(answerArray[6]);
			int enteredAnswerDD = int.Parse(answerArray[7]);
			int enteredAnswerE = int.Parse(answerArray[8]);
			int enteredAnswerEE = int.Parse(answerArray[9]);

			responseA = getResponse (expectedAnswerA, expectedAnswerAA, enteredAnswerA, enteredAnswerAA);
			responseB = getResponse (expectedAnswerB, expectedAnswerBB, enteredAnswerB, enteredAnswerBB);
			responseC = getResponse (expectedAnswerC, expectedAnswerCC, enteredAnswerC, enteredAnswerCC);
			responseD = getResponse (expectedAnswerD, expectedAnswerDD, enteredAnswerD, enteredAnswerDD);
			responseE = getResponse (expectedAnswerE, expectedAnswerEE, enteredAnswerE, enteredAnswerEE);

			if (!responseA || !responseB || !responseC || !responseD || !responseE) {
				response = 0;
				responseText = "error8Level2";
			} else {
				response = 1;
				responseText = "reward8Level2";
			}
		}
		myobjects.Add (response.ToString ());
		myobjects.Add (responseText);
		return myobjects;
	}

	bool getResponse(int expectedAnswerA, int expectedAnswerAA, int enteredAnswerA, int enteredAnswerAA){
		bool response = false;
		if (enteredAnswerA >= expectedAnswerA && enteredAnswerAA <= expectedAnswerAA) {
			response = true;
			GameManager.Instance.QuestionsStatus.Add (response.ToString());

		} else {
			response = false;
			GameManager.Instance.QuestionsStatus.Add (response.ToString());
		}
		return response;
	}
}
