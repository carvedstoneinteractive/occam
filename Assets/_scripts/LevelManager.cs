﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	public GameObject level1Object;
	public GameObject level2Object;
	public GameObject level3Object;
	public bool level1 = false;
	public bool level2 = false;
	public bool level3 = false;

	void Start () {
		level1 = GameManager.Instance.sessionStorage.onGetLevel1Result ();
		level2 = GameManager.Instance.sessionStorage.onGetLevel2Result ();
		level3 = GameManager.Instance.sessionStorage.onGetLevel3Result ();

		//level1Object.SetActive (level1);
		level2Object.SetActive (level2);
		level3Object.SetActive (level3);
	}

	public void LoadLevelSelected(int level){
		SceneManager.LoadScene (level);
	}
}
