﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonObject : MonoBehaviour {

	public bool isInteractable = false;	

	public void Awake()
	{
		ButtonManager.OnAnswer += this.Answer;
		ButtonManager.OnDontAnswer += this.DontAnswer;
	}

	public void OnDestroy()
	{
		ButtonManager.OnAnswer -= this.Answer;
		ButtonManager.OnDontAnswer -= this.DontAnswer;
	}

	public void Answer()
	{
		if (GetComponent<Toggle> () != null) {
			GetComponent<Toggle> ().interactable = true;
		} else {
			GetComponent<Button> ().interactable = true;
		}
	}

	public void DontAnswer()
	{
		if (GetComponent<Toggle> () != null) {
			GetComponent<Toggle> ().interactable = false;
		} else {
			GetComponent<Button> ().interactable = false;
		}
	}
}
