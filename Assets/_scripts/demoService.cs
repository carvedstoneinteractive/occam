﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class demoService {

	public List<string> myobjects = new List<string>();
	public List<string> getAnswer(string answer,int demoQuestion){
		int response = 0;
		string responseText = "";
		if (demoQuestion == 0) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward1Demo";
			} else {
				response = 0;
				responseText = "error1Demo";
			}
		}

		if (demoQuestion == 1) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward2Demo";
			} else {
				response = 0;
				responseText = "error2Demo";
			}
		}

		if (demoQuestion == 2) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray [0]) >= 95 && int.Parse(answerArray [1]) >= 95) {
				response = 1;
				responseText = "reward3Demo";
			} else {
				response = 0;
				responseText = "error3Demo";
			}
		}
		myobjects.Add (response.ToString ());
		myobjects.Add (responseText);
		return myobjects;
	}
}
