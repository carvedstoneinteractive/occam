﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class probabilityService {
	int response;
	string responseText = "";
	public List<string> myobjects = new List<string>();
	string responseTrue = "true";
	string responseFalse = "false";

	public List<string> getAnswer(string answer,int currentQuestion){
		
		if (currentQuestion == 0) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward1Level1";
			} else {
				response = 0;
				responseText = "error1Level1";
			}
		}

		if (currentQuestion == 1) {
			string[] answerArray = answer.Split (',');
			float expectedAnswer = 4f / 20f;
			//float givenAnswer = float.Parse(answerArray [0]) / 20f;
			float enteredAnswer = float.Parse (answerArray [1]);
			if (expectedAnswer != enteredAnswer ) {
				response = 0;
				responseText = "error2Level1";
			} else {
				response = 1;
				responseText = "reward2Level1";
			}
		}

		if (currentQuestion == 2) {
			bool responseA = false;
			bool responseB = false;
			bool responseC = false;

			string[] answerArray = answer.Split (',');
			float expectedAnswerA = 11f / 20f;
			float expectedAnswerB = 2f / 2f;
			float expectedAnswerC = 3f + 5f;
			expectedAnswerC = expectedAnswerC / 4f;

			float enteredAnswerA = float.Parse(answerArray[1]);
			float enteredAnswerB = float.Parse(answerArray[4]);
			float enteredAnswerC = float.Parse(answerArray[8]);

			responseA = findResponseBooleans (enteredAnswerA, expectedAnswerA);
			responseB = findResponseBooleans (enteredAnswerB, expectedAnswerB);
			responseC = findResponseBooleans (enteredAnswerC, expectedAnswerC);

			if (!responseA || !responseB || !responseC) {
				response = 0;
				responseText = "error3Level1";
			} else {
				response = 1;
				responseText = "reward3Level1";
			}
		}

		if (currentQuestion == 3) {
			bool responseA = false;
			bool responseB = false;
			bool responseC = false;

			string[] answerArray = answer.Split (',');
			float expectedAnswerA = 6f / 4f;
			float expectedAnswerB = 4f / 500f;
			float enteredAnswerA = float.Parse(answerArray[0]);
			float enteredAnswerB = float.Parse(answerArray[1]);
			responseA = findResponseBooleans (enteredAnswerA, expectedAnswerA);
			responseB = findResponseBooleans (enteredAnswerB, expectedAnswerB);
			if (float.Parse(answerArray [2]) >= 0.45f && float.Parse(answerArray [2]) <= 0.50f) {
				responseC = true;
			}

			if (!responseA || !responseB || !responseC) {
				response = 0;
				responseText = "error4Level1";
			} else {
				response = 1;
				responseText = "reward4Level1";
			}


		}

		if (currentQuestion == 4) {
			string[] answerArray = answer.Split (',');
			if (int.Parse(answerArray[0])==1) {
				response = 1;
				responseText = "reward5Level1";
			} else {
				response = 0;
				responseText = "error5Level1";
			}
		}

		if (currentQuestion == 5) {
			bool responseA = false;
			bool responseB = false;
			bool responseC = false;

			string[] answerArray = answer.Split (',');
			float enteredAnswerA = float.Parse(answerArray[0]);
			float enteredAnswerB = float.Parse(answerArray[1]);
			float enteredAnswerC = float.Parse(answerArray[1]);
			if (enteredAnswerA >= 40f && enteredAnswerA <= 100f) {
				responseA = true;
			}

			if (enteredAnswerB >= 0.4f && enteredAnswerB <= 0.5f) {
				responseB = true;
			}

			if (enteredAnswerC >= 0.4f && enteredAnswerC <= 0.5f) {
				responseC = true;
			}
			if (!responseA || !responseB || !responseC) {
				response = 0;
				responseText = "error6Level1";
			} else {
				response = 1;
				responseText = "reward6Level1";
			}
		}
		if (currentQuestion == 6) {
			bool responseA = false;
			bool responseB = false;
			bool responseC = false;
			string[] answerArray = answer.Split (',');
			float enteredAnswerA = float.Parse(answerArray[0]);
			float enteredAnswerB = float.Parse(answerArray[1]);
			float enteredAnswerC = float.Parse(answerArray[2]);

			if (enteredAnswerA >= 10f && enteredAnswerA <= 50f) {
				responseA = true;
			}

			if (enteredAnswerB >= 0.4f && enteredAnswerB <= 0.5f) {
				responseB = true;
			}

			if (enteredAnswerC >= 0.4f && enteredAnswerC <= 0.5f) {
				responseC = true;
			}
			if (!responseA || !responseB || !responseC) {
				response = 0;
				responseText = "error7Level1";
			} else {
				response = 1;
				responseText = "reward7Level1";
			}
		}

		myobjects.Add (response.ToString ());
		myobjects.Add (responseText);
		return myobjects;
	}


	bool findResponseBooleans(float enteredAnswer, float expectedAnswer){
		if (enteredAnswer != expectedAnswer) {
			return false;
		} else {
			return true;
		}
	}
}
