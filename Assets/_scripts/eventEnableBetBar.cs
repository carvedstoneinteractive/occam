﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eventEnableBetBar : MonoBehaviour {


	void Start () {
		if (GetComponent<Toggle> () != null) {
			Toggle b = gameObject.GetComponent<Toggle> ();
			b.onValueChanged.AddListener(delegate { ButtonManager.showBetBar(); });
		} else {
			Button b = gameObject.GetComponent<Button> ();
			b.onClick.AddListener (() => ButtonManager.showBetBar());
		}
	}
}
