﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class chipsManager : MonoBehaviour {
	public Slider playerChipsBar;
	public int playerChipsAmountBet;
	public TextMeshProUGUI playerChipsBarText;

	void Start(){
		playerChipsBar.value = GameManager.Instance.playerChips;
		playerChipsBar.onValueChanged.AddListener(delegate {ValueChangeCheck(); });
		playerChipsBarText.text = playerChipsBar.value.ToString ();
	}

	void ValueChangeCheck(){
		playerChipsBarText.text = GameManager.Instance.playerChips.ToString ();
	}

	public void gainChips(){
		ButtonManager.dontanswer ();
		playerChipsBar.value += GameManager.Instance.placedBet;
		GameManager.Instance.playerChips += GameManager.Instance.placedBet;
		GameManager.Instance.placedBet = 0;
		playerChipsBarText.text = GameManager.Instance.playerChips.ToString ();

	}

	public void lostChips(){
		ButtonManager.dontanswer ();
		playerChipsBar.value -= GameManager.Instance.placedBet;
		GameManager.Instance.playerChips -= GameManager.Instance.placedBet;
		GameManager.Instance.placedBet = 0;
		playerChipsBarText.text = GameManager.Instance.playerChips.ToString ();
	}
}
