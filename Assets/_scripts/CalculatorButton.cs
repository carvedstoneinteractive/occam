﻿using UnityEngine;
using System.Collections;

public class CalculatorButton : MonoBehaviour {

	public Calculator _calculator;
	public string _value;

	public void SetInputValue(){
		_calculator.SetInputValue (_value);
	}
}
