﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eventDisableBetBar : MonoBehaviour {


	void Start () {
		Button b = gameObject.GetComponent<Button> ();
		b.onClick.AddListener (() => ButtonManager.hideBetBar());
	}
}
