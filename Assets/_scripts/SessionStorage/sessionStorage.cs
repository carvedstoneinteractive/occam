﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sessionStorage  {

	public void onSetUserName(string name){
		PlayerPrefs.SetString ("username", name);
	}

	public string onGetUserName(){
		return PlayerPrefs.GetString ("username");
	}

	public void onSetLevel1Result(){
		PlayerPrefs.SetString ("level1", "true");
	}

	public void onSetLevel2Result(){
		PlayerPrefs.SetString ("level2", "true");
	}

	public void onSetLevel3Result(){
		PlayerPrefs.SetString ("level3", "true");
	}

	public bool onGetLevel1Result(){
		if (PlayerPrefs.GetString ("level1") == "") {
			return false;
		} else {
			return true;
		}
	}

	public bool onGetLevel2Result(){
		if (PlayerPrefs.GetString ("level2") == "") {
			return false;
		} else {
			return true;
		}	
	}

	public bool onGetLevel3Result(){
		if (PlayerPrefs.GetString ("level3") == "") {
			return false;
		} else {
			return true;
		}	
	}

}
