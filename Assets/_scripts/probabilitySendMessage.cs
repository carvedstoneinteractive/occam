﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class probabilitySendMessage : MonoBehaviour {

	public void storeAnswer(string answer){
		GameManager.Instance.selectedAnswer = answer;
	}

	public void probabilityButtonSendMessage(string answer){
		GameManager.Instance.probabilityReceiver (answer);
	}

	public void conficenceButtonSendMessage(string answer){
		GameManager.Instance.confidenceReceiver (answer);
	}

	public void demoSendMessage(string answer){
		GameManager.Instance.demoReceiver (answer);
	}

	public void probabilityDemoReward(){
		GameManager.Instance.sessionStorage.onSetLevel2Result();
		GameManager.Instance.promotionReceiver ();
	}

	public void confidenceDemoReward(){
		GameManager.Instance.sessionStorage.onSetLevel3Result();
		GameManager.Instance.promotionReceiver ();
	}

	//when 2 inputs are present
	public void impactAmountSendMessage(){
		List<int> answer = new List<int>();
		answer.Add (int.Parse (GameObject.Find ("lowerBoundValue").GetComponent<Text> ().text));
		answer.Add (int.Parse (GameObject.Find ("upperBoundValue").GetComponent<Text> ().text));

		GameManager.Instance.impactReceiverDemo (answer);
	}
}
