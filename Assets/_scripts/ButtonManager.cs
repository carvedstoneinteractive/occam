﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {
	public delegate void ButtonHandler();
	public static event ButtonHandler OnAnswer;
	public static event ButtonHandler OnDontAnswer;
	public static event ButtonHandler OnshowBetBar;
	public static event ButtonHandler OnhideBetBar;
	public static event ButtonHandler OnSendAnswer;



	public static void answer()
	{
		if (OnAnswer != null) {
			OnAnswer ();
		}
	}

	public static void dontanswer()
	{
		if (OnDontAnswer != null) {
			OnDontAnswer ();
		}
	}

	public static void showBetBar()
	{
		if (OnshowBetBar != null) {
			OnshowBetBar ();
		}
	}

	public static void hideBetBar()
	{
		if (OnhideBetBar != null) {
			OnhideBetBar ();
		}
	}

	public static void sendAnswer()
	{
		if (OnSendAnswer != null) {
			OnSendAnswer ();
		}
	}
}