﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class errorManager : MonoBehaviour {

	public GameObject humbleMomentObject;
	public GameObject rewardMomentObject;
	public GameObject promotionObject;
	public GameObject endGameObject;

	public TextMeshProUGUI errorText;
	public TextMeshProUGUI rewardText;
	public TextMeshProUGUI[] level2Q7Answers;
	public TextMeshProUGUI[] endGameText;
	public void getLocalizedError(string errorNumber){
		string[] answerArray = errorNumber.Split (',');
		if (answerArray.Length > 1) {
			for (int i = 0; i < answerArray.Length; i++) {
				endGameText [i].text = changeLang.Instance.LMan.getString (answerArray [i]);
			}
		} else {
			errorText.text = changeLang.Instance.LMan.getString(errorNumber);
		}
		GameObject.Find ("chipsBar").GetComponent<chipsManager> ().lostChips ();
	}

	public void getLocalizedReward(string rewardNumber){
		string[] answerArray = rewardNumber.Split (',');
		rewardText.text = changeLang.Instance.LMan.getString(answerArray[0]);
		GameObject.Find ("chipsBar").GetComponent<chipsManager> ().gainChips ();
	}

	public void CheckPlayerChips(){
		if (GameManager.Instance.playerChips <= 0) {
			GameManager.Instance.playerChips = 200;
			promotionLoadNextLevel.Instance.loadSceneSelect ();
		}
	}
}
