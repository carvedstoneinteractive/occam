﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetBarManager : MonoBehaviour {

	public GameObject betBarObject;

	public void Awake()	{
		ButtonManager.OnshowBetBar += this.ShowBetBar;
		ButtonManager.OnhideBetBar += this.HideBetBar;
	}

	public void OnDestroy()	{
		ButtonManager.OnshowBetBar -= this.ShowBetBar;
		ButtonManager.OnhideBetBar += this.HideBetBar;
	}

	void ShowBetBar(){
		if (!betBarObject) {
			GameObject.Find ("betBarDiv").GetComponent<BetBarManager> ().betBarObject.SetActive (true);
		} else {
			betBarObject.SetActive (true);
		}
	}

	public void HideBetBar(){
		if (!betBarObject) {
			GameObject.Find ("betBarDiv").GetComponent<BetBarManager> ().betBarObject.SetActive (false);
		} else {
			betBarObject.SetActive (false);
		}
	}
}
