﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChipsCounter : MonoBehaviour {
	public int score = 0;
	public int playerChips = 0;
	TextMeshProUGUI scoreCounter;

	void Start () {
		scoreCounter = gameObject.GetComponent<TextMeshProUGUI> ();
		playerChips = GameManager.Instance.playerChips;
		StartCoroutine (Counter (playerChips));
	}

	public void DisableCounter(){
		StopAllCoroutines ();
		scoreCounter.text = GameManager.Instance.playerChips.ToString("N0");
	}

	IEnumerator Counter(int count)
	{
		for(int i = 0; i < count; i++)
		{
			score++;
			scoreCounter.text = score.ToString ("N0");
			yield return new WaitForSeconds(0.5f/count);
		}
	}
}
