﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderEffect : MonoBehaviour {

	public TextMeshProUGUI[] texts;
	public Slider slider;
	public float normalValue = 30f;
	public float highlightedValue = 35f;
	void Start () {
		texts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
		slider = gameObject.GetComponent<Slider>();
		slider.onValueChanged.AddListener (delegate {
			ValueChanged ();
		});
	}
	
	void ValueChanged () {
		if (slider.value <= 0.25f) {
			SetFontSizeBig (texts [0]);
		}
		if (slider.value >= 0.25f && slider.value <= 0.50f) {
			SetFontSizeBig (texts [1]);
		}
		if (slider.value >= 0.50f && slider.value <= 0.75f) {
			SetFontSizeBig (texts [2]);
		}
		if (slider.value >= 0.75f && slider.value <= 1.0f) {
			SetFontSizeBig (texts [3]);
		}
		if (slider.value >= 1f) {
			SetFontSizeBig (texts [4]);
		}
		float value = Mathf.Round (slider.value / 0.25f) * 0.25f;
		slider.value = value;
		ButtonManager.showBetBar ();
	}

	void SetFontSizeBig(TextMeshProUGUI text){
		text.fontSize = highlightedValue;
		text.fontStyle = FontStyles.Bold;
		for (int i = 0; i < texts.Length; i++) {
			if (texts [i] != text) {
				texts [i].fontSize = normalValue;
				texts[i].fontStyle = FontStyles.Normal;

			}
		}
	}
}
