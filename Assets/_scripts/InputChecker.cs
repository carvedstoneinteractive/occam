﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputChecker : MonoBehaviour {
	public InputField[] inputField;
	public Slider[] sliders;
	public Toggle[] toggles;
	public List<string> inputs = new List<string>();
	public Button btnAccept;

	public void Start()
	{
		for (int i = 0; i < inputField.Length; i++) {
			inputField[i].onEndEdit.AddListener(delegate { pepe(); });
		}
		if (toggles.Length > 0) {
			for (int i = 0; i < toggles.Length; i++) {
				toggles[i].onValueChanged.AddListener(delegate { pepe(); });
			}
		}
		if (sliders.Length > 0) {
			for (int i = 0; i < sliders.Length; i++) {
				sliders[i].onValueChanged.AddListener(delegate { pepe(); });
			}
		}
	}

	void pepe(){
		GameManager.Instance.selectedAnswer = "";
		inputs.Clear ();

		for (int i = 0; i < inputField.Length; i++) {
			inputs.Add (inputField [i].text);
		}
		for (int i = 0; i < sliders.Length; i++) {
			inputs.Add (sliders [i].value.ToString());
		}
		if (toggles.Length > 0) {
			for (int i = 0; i < toggles.Length; i++) {
				if (toggles [i].isOn) {
					inputs.Add ("1");
				}else{
					inputs.Add ("0");
				}
			}
		}
		if (!inputs.Contains ("")) {
			GameManager.Instance.selectedAnswer = string.Join(",",inputs.ToArray());
			ButtonManager.showBetBar ();
		} else {
			inputs.Clear ();
		}
	}
}
