﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerBetSender : MonoBehaviour {
	public float amount = 0;

	void Start(){
		Button b = gameObject.GetComponent<Button> ();
		b.onClick.AddListener (() => ButtonManager.answer());
	}

	public void sendBet(int number){
		amount = number / 100f; 
		amount = amount  * GameManager.Instance.playerChips;
		GameManager.Instance.placedBet = (int)amount;


	}
}
